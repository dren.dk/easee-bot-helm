# Helm chart for easee-bot

This is the helm chart that deploys the easee-bot daemon.


## Secret creation

```shell
kubectl create secret generic secret --from-literal=easee.user=... --from-literal=easee.password=... --from-literal=easee.site-id=...
```
